from django.contrib import admin
from apps.products.models import *
# Register your models here.

class UnitAdmin(admin.ModelAdmin):
    list_display = ('id', 'description')
    icon_name = 'straighten'

class CategoryProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'description')
    icon_name = 'card_travel'

class ProductAdmin(admin.ModelAdmin):

    icon_name = 'devices'

admin.site.register(UnitModel, UnitAdmin)
admin.site.register(CategoryProductModel, CategoryProductAdmin)
admin.site.register(OfferModel)
admin.site.register(ProductModel, ProductAdmin)

#---------------------------------------------------
# Personalizacion de cabecera de Administración
admin.site.site_header = 'Sistema de Comercio'
admin.site.index_title = 'Administración de sistema'
admin.site.site_title = 'Modulo V'

