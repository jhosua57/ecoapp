from django.db import models
from simple_history.models import HistoricalRecords
from apps.base.models import BaseModel
# Create your models here.

class UnitModel(BaseModel):
    description = models.CharField('Descripción', max_length=50, blank=False, null=False, unique=True)
    historical = HistoricalRecords()

    class Meta:
        verbose_name = 'Unidad de medida'
        verbose_name_plural = 'Unidades de medida'
        
    def __str__(self):
        return self.description


class CategoryProductModel(BaseModel):
    description = models.CharField('Descripción', max_length=50, unique=True, null=False, blank=False)
    
    class Meta:
        """Meta definition for CategoriaProducto."""
        verbose_name = 'Categoria de Producto'
        verbose_name_plural = 'Categorias de Productos'

    def __str__(self):
        """Unicode representation of CategoriaProducto."""
        return self.description

class OfferModel(BaseModel):
    descuento = models.PositiveSmallIntegerField(default = 0)
    categoria_producto = models.ForeignKey(CategoryProductModel, on_delete=models.CASCADE, verbose_name='Indicador de oferta')

    historical = HistoricalRecords()
    
    class Meta:
        """Meta definition for Offer."""

        verbose_name = 'Indicador de oferta'
        verbose_name_plural = 'Indicadores de ofertas'

    def __str__(self):
        """Unicode representation of Offer."""
        return f'Oferta de la categoria {self.categoria_producto}:{self.descuento}%'
    
class ProductModel(BaseModel):
    
    name = models.CharField('Nombre de producto', max_length=150, unique=True, blank=False, null=False)
    description = models.TextField('Descripcion de producto', blank=False, null=False)
    image = models.ImageField('Imagen del producto', upload_to='products/', blank=True, null=True, height_field=None, width_field=None, max_length=None)
    unit_measurement = models.ForeignKey(UnitModel, on_delete=models.CASCADE, verbose_name='Unidad de medida', null= True)
    category_product = models.ForeignKey(CategoryProductModel, on_delete=models.CASCADE, verbose_name='Categoria de Producto', null= True)
    historical = HistoricalRecords()

    class Meta:
        """Meta definition for Product."""

        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'

    def __str__(self):
        """Unicode representation of Product."""
        return self.name



