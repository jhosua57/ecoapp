from rest_framework.routers import DefaultRouter

from apps.products.api.views.products_viewsets import ProductViewSet
from apps.products.api.views.general_views import *

router = DefaultRouter()

router.register(r'products', ProductViewSet, basename='products')
router.register(r'unidad_medida', UnitViewSet, basename='unidad_medida')
router.register(r'ofertas', OfferViewSet, basename='ofertas')
router.register(r'categoria_producto', CategoryProductViewSet, basename='categoria_producto')
urlpatterns = router.urls
