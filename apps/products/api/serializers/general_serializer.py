from pyexpat import model
from apps.products.models import *
from rest_framework import serializers


class UnitSerializer(serializers.ModelSerializer):
    class Meta:
        model = UnitModel
        exclude = ('state', 'created_date', 'modified_date', 'deleted_date')

class CategoryProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategoryProductModel
        exclude = ('state', 'created_date', 'modified_date', 'deleted_date')
    
class OfferSerializer(serializers.ModelSerializer):
    class Meta:
        model = OfferModel
        exclude = ('state', 'created_date', 'modified_date', 'deleted_date')
