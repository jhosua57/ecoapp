from rest_framework import serializers

from apps.products.models import ProductModel

class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductModel
        exclude = ('state', 'created_date', 'modified_date', 'deleted_date')
    
    def to_representation(self, instance):
        return {
            'id': instance.id,
            'name': instance.name,
            'description': instance.description,
            'image': instance.image if instance.image != '' else '',
            'unit_measurement': instance.unit_measurement.description if instance.unit_measurement is not None else '',
            'category_product': instance.category_product.description if instance.category_product is not None else ''
        }
