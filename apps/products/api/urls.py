from django.urls import path
from apps.products.api.views.general_views import *
from apps.products.api.views.products_viewsets import *

""" urlpatterns = [
    path('unidad_medida/', ListaUnidadMedidaAPIView.as_view(), name='unidad_medida'),
    path('indicador/', ListaIndicadorAPIView.as_view(), name='indicador'),
    path('categoria_producto/', ListaCategoriaProductoAPIView.as_view(), name='categoria_producto'),
]
 """