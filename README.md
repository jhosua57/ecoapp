# ECOAPP



## Datos personales

Proyecto elaborado por Jose Oña Leaño

## Instalacion del proyecto

Para instalar debe crear un entorno virtual para mayor seguridad y estabilidad bajo el siguiente comando:

```
python -m venv env

```

## Activar el entorno virtual creado

Introducir el siguiente comando para la activacion del entorno virtual:

### Windows

```
\env\Scripts\activate

```
### Linux/Mac

```
. env/bin/activate

```

## Instalación de contenido de librerias  

Introducir el siguiente comando para la instalación de las librerias necesarias para el proyecto:

```
pip install -r requirements.txt

```

## Ejecutar el proyecto

Introducir el siguiente comando para ejecutar el proyecto:

```
python manage.py runserver

```


# Editing la conexion de base de datos

El sistema esta desarrollado para el funcionamiento de diferentes base de datos SQLite, Postgresql, etc

Para cambiar la conexion de la base de datos el archivo lo encontrará en el siguiente directorio:

```
- ecoapp
  - config
    - settings
      - local
```


